<?php
/**
 * @file
 * API documentation for hooks provided by the Commerce Card On File terminal
 * module.
 */

/**
 * Lets modules alter the cards on file to presented on the payment terminal
 *
 * For orders already associated with a payment instance, then cof terminal
 * module will initialize the terminal options with the order owner's cards
 * associated with that payment method instance id via
 * $order->data['payment_method']
 *
 * For orders that have no payment method instance association, then the
 * card options are initialized to all of the order owner's cards on file
 * for all payment method instances.  This can commonly occur for manually
 * created orders or automatically generated orders with no
 * $order->data['payment_method'].
 *
 * @param array $stored_cards
 *   An array of card data keyed by card_id
 * @param stdClass $order
 *   The order object
 */
function hook_commerce_cof_terminal_stored_cards_alter(&$stored_cards, $order) {
  $my_instance_id = my_module_resolve_payment_instance($order);
  foreach (array_keys($stored_cards) as $card_id) {
    if ($stored_cards[$card_id]['instance_id'] != $my_instance_id) {
      unset($stored_cards[$card_id]);
    }
  }
}
